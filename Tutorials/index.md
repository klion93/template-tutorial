# Welcome to the Template Tutorial Project!

This will be your home page

## How to use this documentation

In the left side panel of this site you find the sections of you site. On the right side you find a table of contents of the current page.

## What is currently supported

All files should have the ending ".md" and use the Markdown syntax. Additionally, latex math expressions, such as `$ ...$` are rendered as well.
