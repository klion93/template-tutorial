# Template Tutorial

This is a template tutorial for everyone interested in contributing FHI-aims tutorials to this group. This project provides all the setup needed for the generation of GitLab pages with mkdocs-material. Feel free to copy-and-paste or fork this project for your own purposes. Please find this web page here:

https://fhi-aims-club.gitlab.io/tutorials/template-tutorial

# Things to change

The most important things to change are marked with **# CHANGME** in the `mkdocs.yml` file. They are related to links and the name of the project. 
